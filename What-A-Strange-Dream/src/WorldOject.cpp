#include "WorldObject.hpp"

WorldObject::WorldObject(){}

WorldObject::WorldObject(unsigned int id, float offsetX, float offsetZ):worldId(id), worldOffsetX(offsetX), worldOffsetZ(offsetZ){}

WorldObject::WorldObject(const WorldObject &wo):worldId(wo.worldId), worldOffsetX(wo.worldOffsetX), worldOffsetZ(wo.worldOffsetZ){}

WorldObject::~WorldObject(){}

unsigned int WorldObject::getWorldId(){
	return worldId;
}