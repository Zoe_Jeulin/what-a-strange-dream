#pragma once

#include "Model.hpp"
#include "Light.hpp"
#include "Skybox.hpp"
#include "Music.hpp"
#include "Leaf.hpp"
#include <GL/glew.h>
#include <iostream>
#include <glimac/glm.hpp>
#include <glimac/Program.hpp>
#include <vector>

class World{

    private:
        unsigned int worldId;
        std::vector<Model> worldModels;
        std::vector<Light> worldLights;
        Skybox worldSkybox;
        Music worldMusic;

    public:
        World(unsigned int id, std::vector<Model> &models, std::vector<Light> &lights, Skybox &skybox, Music &music);
        void drawWorld(glimac::Program &program, glm::mat4 const &viewMatrix, std::vector<Leaf> &leaves);
};