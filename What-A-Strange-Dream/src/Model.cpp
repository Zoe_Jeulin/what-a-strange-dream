#include "Model.hpp"

GLuint TextureFromFile(const char *path, const std::string &directory);

Model::Model(unsigned int id, std::string const &path, float offsetX, float offsetZ):WorldObject(id, offsetX, offsetZ){
	this->worldId=id;
	this->worldOffsetX=offsetX;
	this->worldOffsetZ=offsetZ;
	this->path=path;
	loadModel(path, offsetX, offsetZ);
}

//Dessine le Model
void Model::drawModel(glimac::Program &program){
	for(unsigned int i = 0; i < meshes.size(); i++){
        meshes[i].drawMesh(program);
    }
}

//Charge le Model, via Assimp
void Model::loadModel(std::string const &path, float offsetX, float offsetZ){
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals);
    // check for errors
    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
    {
        std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
        return;
    }
    // retrieve the directory path of the filepath
    directory = path.substr(0, path.find_last_of('/'));

    // process ASSIMP's root node recursively
    processNode(scene->mRootNode, scene, offsetX, offsetZ);
}

//Charge le Model, via Assimp
void Model::processNode(aiNode *node, const aiScene *scene, float offsetX, float offsetZ){
    // process each mesh located at the current node
    for(unsigned int i = 0; i < node->mNumMeshes; i++){
        // the node object only contains indices to index the actual objects in the scene. 
        // the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(processMesh(mesh, scene, offsetX, offsetZ));
    }
    // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
    for(unsigned int i = 0; i < node->mNumChildren; i++){
        processNode(node->mChildren[i], scene, offsetX, offsetZ);
    }
}

//Charge le Model, via Assimp
Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene, float offsetX, float offsetZ){
    // data to fill
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;

    // walk through each of the mesh's vertices
    for(unsigned int i = 0; i < mesh->mNumVertices; i++){
        Vertex vertex;
        glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
        // positions
        vector.x = mesh->mVertices[i].x+offsetX;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z+offsetZ;
        vertex.Position = vector;
        // normals
        if(mesh->HasNormals()){
            vector.x = mesh->mNormals[i].x;
            vector.y = mesh->mNormals[i].y;
            vector.z = mesh->mNormals[i].z;
            vertex.Normal = vector;
        }
        // texture coordinates
        if(mesh->mTextureCoords[0]){ // does the mesh contain texture coordinates?
            glm::vec2 vec;
            // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
            vec.x = mesh->mTextureCoords[0][i].x; 
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        }
        else{
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);
        }

        vertices.push_back(vertex);
    }
    // now walk through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
    for(unsigned int i = 0; i < mesh->mNumFaces; i++){
        aiFace face = mesh->mFaces[i];
        // retrieve all indices of the face and store them in the indices vector
        for(unsigned int j = 0; j < face.mNumIndices; j++){
            indices.push_back(face.mIndices[j]);        
        }
    }
    
    // process materials
    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
	Material mat;
	aiColor3D color;

	material->Get(AI_MATKEY_COLOR_AMBIENT, color);
	mat.Kd = glm::vec4(color.r, color.g, color.b,1.0);
	material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
	mat.Ka = glm::vec4(color.r, color.g, color.b,1.0);
	material->Get(AI_MATKEY_COLOR_SPECULAR, color);
	mat.Ks = glm::vec4(color.r, color.g, color.b,1.0);

    return Mesh(vertices, indices, textures, mat);
}

//Charge le Model, via Assimp
GLuint TextureFromFile(const char *path, const std::string &directory){
    std::string filename = std::string(path);
    filename = directory + '/' + filename;

    glimac::Image *texture = glimac::loadImage(filename);
    if(texture==nullptr){
        std::cout << "Texture failed to load at path: " << path << std::endl;
    }

    GLuint textureId;

    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->getWidth(), texture->getHeight(), 0, GL_RGBA, GL_FLOAT, texture->getPixels());
    glBindTexture(GL_TEXTURE_2D, 0);

    return textureId;
}

std::string Model::getPath(){
	return path;
}

//Libère la mémoire réservée par les Model à la fin du programme
void Model::deleteData(){
	for(unsigned int i=0; i<meshes.size(); i++){
		glDeleteVertexArrays(1, meshes[i].getVAO());
    	glDeleteBuffers(1, meshes[i].getVBO());
	}
}