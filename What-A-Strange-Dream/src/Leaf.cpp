#include "Leaf.hpp"

Leaf::Leaf(){}

Leaf::Leaf(unsigned int id, glm::vec3 pos, int index, bool shown, bool collected, float offsetX, float offsetZ):WorldObject(id, offsetX, offsetZ){
	this->worldId=id;
	this->leafPos=pos;
	leafPos.x+=offsetX;
	leafPos.z+=offsetZ;
	this->indexModel=index;
	this->shown=shown;
	this->collected=collected;
	this->worldOffsetX=offsetX;
	this->worldOffsetZ=offsetZ;
}

Leaf::Leaf(const Leaf &lf):WorldObject(lf){
	this->worldId=lf.worldId;
	this->leafPos=lf.leafPos;
	this->indexModel=lf.indexModel;
	this->shown=lf.shown;
	this->collected=lf.collected;
	this->worldOffsetX=lf.worldOffsetX;
	this->worldOffsetZ=lf.worldOffsetZ;
}

int Leaf::getIndexModel(){
	return indexModel;
}

void Leaf::setIndexModel(int index){
	indexModel=index;
}

bool Leaf::getShown(){
	return shown;
}

void Leaf::setShown(bool s){
	shown=s;
}

bool Leaf::getCollected(){
	return collected;
}

void Leaf::setCollected(bool c){
	collected=c;
}

//Retourne un booléen indiquant si le joueur est suffisament proche de la feuille pour l'attraper
bool Leaf::isClose(glm::vec3 posCamera){
	if(glm::distance(glm::vec2(leafPos.x,leafPos.z),glm::vec2(posCamera.x,posCamera.z))<2){
		return true;
	}else{
		return false;
	}
}