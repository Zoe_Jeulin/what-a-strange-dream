#pragma once

class WorldObject{
	protected:
	    unsigned int worldId;
	    float worldOffsetX;
	    float worldOffsetZ;

    public:
        WorldObject();
        WorldObject(unsigned int worldId, float offsetX, float offsetZ);
        WorldObject(const WorldObject &wo);
        virtual ~WorldObject();
        unsigned int getWorldId();
};