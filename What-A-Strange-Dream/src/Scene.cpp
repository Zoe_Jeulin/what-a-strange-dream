#include "Scene.hpp"

Scene::Scene(std::string const &path){
	loadScene(path);
}

//Charge la scène via le fichier de description de scène
void Scene::loadScene(std::string const &path){
	std::stringstream stream;
	std::ifstream sceneFile(path);
	std::string line = "";
	std::string prefix = "";
	std::string pathFolder, pathFile;
	unsigned int worldId;
	float offsetX, offsetZ;
	float gatePosXmain, gatePosZmain, gateRadius, gatePosXworld, gatePosZworld;
	glm::vec3 coordsPos, lightIntensity;
	glm::vec3 leafPos;
	bool leafShown;

	//Vérification de la bonne ouverture du fichier
	if(!sceneFile.is_open()){
		std::cerr << "Erreur au chargement du fichier de scene " << path << std::endl;
        return;
	}
	
	//Lecture du fichier ligne par ligne
	while(std::getline(sceneFile,line)){
		stream.clear();
		stream.str(line);
		stream >> prefix;

		if(prefix== "o"){ //Importation du décalage (offset) de chaque monde dans l'espace
			stream >> worldId;
			stream >> offsetX;
			stream >> offsetZ;
			offsets.push_back(glm::vec3(offsetX,0.,offsetZ));			
		}else if(prefix == "md"){ //Importation des models
			stream >> worldId;
			stream >> pathFolder;

			for (const auto & entry : fs::directory_iterator(PATH_MODELS+pathFolder)){
        		std::cout << "Loading model : " << entry.path() << std::endl; //Commenter pour ne plus voir le nom des models chargés
        		pathFile=entry.path().string();
				if(pathFile.substr(pathFile.length()-3)=="obj"){
					Model newModel(worldId, pathFile, offsets[worldId].x, offsets[worldId].z);
					models.push_back(newModel);
				}
			}			
		}else if(prefix == "li"){ //Importation des lights
			stream >> worldId;
			stream >> coordsPos.x;
			stream >> coordsPos.y;
			stream >> coordsPos.z;
			stream >> lightIntensity.x;

			lightIntensity.x=lightIntensity.x/10.;
			lightIntensity.y = lightIntensity.x;
			lightIntensity.z = lightIntensity.x;
			Light newLight(worldId, coordsPos, lightIntensity, offsets[worldId].x, offsets[worldId].z);
			lights.push_back(newLight);
		}else if(prefix == "sb"){ //Importation des skybox
			stream >> worldId;
			stream >> pathFolder;

			Skybox newSkybox(worldId, PATH_SKYBOXES+pathFolder, offsets[worldId].x, offsets[worldId].z);
			skyboxes.push_back(newSkybox);
		}else if(prefix == "mu"){ //Importation des musiques
			stream >> worldId;
			stream >> pathFile;

			Music newMusic(worldId, PATH_MUSICS+pathFile, offsets[worldId].x, offsets[worldId].z);
			musics.push_back(newMusic);
		}else if(prefix == "g"){ //Importation des infos sur les portails
			stream >> worldId;
			stream >> gatePosXmain;
			stream >> gatePosZmain;
			stream >> gateRadius;
			stream >> gatePosXworld;
			stream >> gatePosZworld;
			gateRadius=gateRadius/2.;
			Gate newGate(worldId, gatePosXmain, gatePosZmain, gateRadius, gatePosXworld, gatePosZworld, offsets[worldId].x, offsets[worldId].z);
			gates.push_back(newGate);
		}else if(prefix == "lf"){ //Importation des infos sur les feuilles
			stream >> worldId;
			stream >> leafPos.x;
			stream >> leafPos.y;
			stream >> leafPos.z;
			stream >> leafShown;

			Leaf newLeaf(worldId, leafPos, -1, leafShown, 0, offsets[worldId].x, offsets[worldId].z);
			leaves.push_back(newLeaf);
		}
	}
	createWorlds();	
}

//Création de chaque monde
void Scene::createWorlds(){
	std::vector<Model> modelsWorld;
    std::vector<Light> lightsWorld;
    int cpt=0;

    for(unsigned int w = 0; w<NB_WORLDS; w++){
    	for(unsigned int m = 0; m<models.size(); m++){
	    	if(models[m].getWorldId()==w){
	    		modelsWorld.push_back(models[m]);
	    	}	    	
	    }

	    for(unsigned int l = 0; l<lights.size(); l++){
	    	if(lights[l].getWorldId()==w){
	    		lightsWorld.push_back(lights[l]);
	    	}
	    }

	    Skybox skyboxWorld=skyboxes[w];
	    Music musicWorld=musics[w];

	    if(w==0){
	    	for(unsigned int i=0; i<modelsWorld.size(); i++){
		    	if(modelsWorld[i].getPath().find("LEAF") != std::string::npos && modelsWorld[i].getPath().find("FLOOR") != std::string::npos){
		    		leaves[w].setIndexModel(i);
	    		}else{
	    			if(modelsWorld[i].getPath().find("LEAF") != std::string::npos){
		    			leaves[w+NB_WORLDS+cpt].setIndexModel(i);
		    			cpt++;
	    			}
	    		}
		    }
	    }else{
	    	for(unsigned int i=0; i<modelsWorld.size(); i++){
    			if(modelsWorld[i].getPath().find("LEAF") != std::string::npos){
	    			leaves[w].setIndexModel(i);
    			}
		    }
	    }

	    World newWorld(w, modelsWorld, lightsWorld, skyboxWorld, musicWorld);
	    worlds.push_back(newWorld);

	    modelsWorld.clear();
	    lightsWorld.clear();
    }
}

//Dessine la scène
void Scene::drawScene(unsigned int idWorld, glimac::Program &program, glm::mat4 const &viewMatrix){
	worlds[idWorld].drawWorld(program, viewMatrix, leaves);
}

//Dessine la skybox du monde actuel
void Scene::drawSkybox(unsigned int idWorld){
	skyboxes[idWorld].drawSkybox();    
}

//Joue la musique du monde actuel
void Scene::playMusic(unsigned int idWorld){
	musics[idWorld].play();
}

//Arrête la musique du monde actuel
void Scene::stopMusic(unsigned int idWorld){
	musics[idWorld].stop();
}

//Verifie si le joueur se trouve dans la zone d'un portail
int Scene::checkGates(glm::vec3 posCamera){
	int idGate=-1;
	for(unsigned int i=0; i<gates.size(); i++){
		if(gates[i].isInMainGate(posCamera)!=-1){
			idGate=i;
		}
	}
	return idGate;
}

Gate* Scene::getGate(int idGate){
	return &gates[idGate];
}

Leaf* Scene::getLeaf(int idLeaf){
	return &leaves[idLeaf];
}

void Scene::updateLeaf(int i){
	getLeaf(i)->setShown(false);
    getLeaf(i+NB_WORLDS)->setShown(true);
    getLeaf(i)->setCollected(true);
}

//Retourne le nombre de feuilles collectées
int Scene::getNbLeavesCollected(){
	int cptCollected=0;
	for(int i=0; i<NB_WORLDS; i++){
		if(leaves[i].getCollected()){
			cptCollected++;
		}
	}
	return cptCollected;
}

//A la fin du programme, libère la mémoire
void Scene::deleteData(){
	for(unsigned int i; i<musics.size(); i++){
		Mix_FreeMusic(musics[i].getMusic());
	}
	Mix_CloseAudio();

	for(unsigned int i; i<models.size(); i++){
		models[i].deleteData();
	}

	for(unsigned int i; i<skyboxes.size(); i++){
		glDeleteVertexArrays(1, skyboxes[i].getVAO());
    	glDeleteBuffers(1, skyboxes[i].getVBO());
	}
}