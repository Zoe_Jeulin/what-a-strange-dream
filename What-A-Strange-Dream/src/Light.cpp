#include "Light.hpp"

Light::Light(unsigned int id, glm::vec3 coords, glm::vec3 intensity, float offsetX, float offsetZ):WorldObject(id, offsetX, offsetZ){
    this->worldId=id;
    this->coordsPos=coords+glm::vec3(offsetX,0.,offsetZ);
    this->lightIntensity=intensity;
    this->worldOffsetX=offsetX;
    this->worldOffsetZ=offsetZ;
}

//Dessine les lumières
void Light::drawLight(glimac::Program &program, glm::mat4 const &viewMatrix, unsigned int i){
    GLint uLightPos = glGetUniformLocation(program.getGLId(), "uLightPos");
    GLint uLightIntensity = glGetUniformLocation(program.getGLId(), "uLightIntensity");

    lightPos=glm::vec3(viewMatrix*glm::vec4(coordsPos,1.));
    
    glUniform3fv(uLightPos,1,glm::value_ptr(lightPos));
    glUniform3fv(uLightIntensity,1,glm::value_ptr(lightIntensity));
}

