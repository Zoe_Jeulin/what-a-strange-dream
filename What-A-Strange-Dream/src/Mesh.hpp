#pragma once

#include "Vertex.hpp"
#include "Texture.hpp"
#include "Material.hpp"
#include <glimac/Program.hpp>
#include <GL/glew.h>
#include <vector>
#include <iostream>

class Mesh{

    private:
        std::vector<Vertex> vertices;
        std::vector<unsigned int> indices;
        std::vector<Texture> textures;
        Material material;

        GLuint VAO, VBO, IBO, UBO;

        void setupMesh();

    public:
        Mesh(std::vector<Vertex> &vertices, std::vector<unsigned int> &indices, std::vector<Texture> &textures, Material &material);
        void drawMesh(glimac::Program &program);
        GLuint* getVAO();
        GLuint* getVBO();
};