#pragma once

#include "Mesh.hpp"
#include <glimac/Program.hpp>
#include <glimac/Image.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <string>
#include <vector>
#include "WorldObject.hpp"

class Model : public WorldObject{
    private:
        std::vector<Mesh> meshes;
        std::string directory;
        std::vector<Texture> textures_loaded;
        std::string path;

        void loadModel(std::string const &path, float offsetX, float offsetZ);
        void processNode(aiNode *node, const aiScene *scene, float offsetX, float offsetZ);
        Mesh processMesh(aiMesh *mesh, const aiScene *scene, float offsetX, float offsetZ);
        std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);

    public:
        Model(unsigned int id, std::string const &path, float offsetX, float offsetZ);
        void drawModel(glimac::Program &program);
        std::string getPath();
        void deleteData();
};

