#include <glimac/SDLWindowManager.hpp>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <glimac/FreeflyCamera.hpp>
#include <glimac/common.hpp>
#include <GL/glew.h>
#include <iostream>
#include "Scene.hpp"


using namespace glimac;

struct SceneProgram{
    Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;

    GLint uFlashlightPos;
    GLint uFlashlightDir;
    GLint uFlashlightCutoff;

    SceneProgram(const FilePath& applicationPath):m_Program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl", applicationPath.dirPath() + "shaders/pointlight2.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
    }
};

struct GrotteProgram{
    Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;

    GLint uLightDir;
    GLint uFlashlightPos;
    GLint uFlashlightDir;
    GLint uFlashlightCutoff;
    GLint uFlashlightOuterCutoff;

    GLint uConstant;
    GLint uLinear;
    GLint uQuadratic;

    GrotteProgram(const FilePath& applicationPath):m_Program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl", applicationPath.dirPath() + "shaders/flashlight.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");

        uLightDir = glGetUniformLocation(m_Program.getGLId(), "uLightDir");
        uFlashlightPos = glGetUniformLocation(m_Program.getGLId(), "uFlashlightPos");
        uFlashlightDir = glGetUniformLocation(m_Program.getGLId(), "uFlashlightDir");
        uFlashlightCutoff = glGetUniformLocation(m_Program.getGLId(), "uFlashlightCutoff");
        uFlashlightOuterCutoff = glGetUniformLocation(m_Program.getGLId(), "uFlashlightOuterCutoff");

        uConstant = glGetUniformLocation(m_Program.getGLId(), "uConstant");
        uLinear = glGetUniformLocation(m_Program.getGLId(), "uLinear");
        uQuadratic = glGetUniformLocation(m_Program.getGLId(), "uQuadratic");
    }
};

struct SkyboxProgram {
    Program m_Program;

    GLint uMVPMatrix;

    GLint uSkybox;

    SkyboxProgram(const FilePath& applicationPath):m_Program(loadProgram(applicationPath.dirPath() + "shaders/skybox.vs.glsl", applicationPath.dirPath() + "shaders/skybox.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrixSkybox");
        uSkybox = glGetUniformLocation(m_Program.getGLId(), "uSkybox");
    }
};

int main(int argc, char** argv) {
    // Initialize SDL and open a window
    SDLWindowManager windowManager(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE);

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();

    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }
    
    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    glEnable(GL_DEPTH_TEST);

    FilePath applicationPath(argv[0]);
    SceneProgram sceneProgram(applicationPath);
    SkyboxProgram skyboxProgram(applicationPath);
    GrotteProgram grotteProgram(applicationPath);

    glm::mat4 ProjMatrix = glm::perspective(glm::radians(70.f), 800.0f/600.0f, 0.1f, 300.0f);
    glm::mat4 MVMatrix = glm::translate(glm::mat4(1), glm::vec3(0.0f, 0.0f, 0.0f));
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));
    glm::mat4 MVPMatrix = ProjMatrix*MVMatrix;

    Scene scene(PATH_ASSETS "scene.txt");

    Mix_Chunk *dingSound = Mix_LoadWAV(PATH_MUSICS "ding.wav");
    Mix_Chunk *footstepsSound = Mix_LoadWAV(PATH_MUSICS "footsteps2.wav");
    Mix_Chunk *tpSound = Mix_LoadWAV(PATH_MUSICS "swoosh.wav");
    Mix_PlayChannel(-1, footstepsSound, -1);
    Mix_VolumeChunk(footstepsSound, MIX_MAX_VOLUME/8);
    SDLKey lastKP;

    int currentWorld=0;
    int gate=-1;
    FreeflyCamera camera;
    glm::ivec2 coordsMouse = windowManager.getMousePosition();
    unsigned int currentTime, lastMove;

    //Boucle principale
    bool done = false;
    while(!done) {
        // Event loop:
        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            if(e.type == SDL_QUIT) {
                done = true;
            }
        }
        if(SDL_GetTicks()-lastMove>150){
            Mix_VolumeChunk(footstepsSound, 0);
        }else{
            Mix_VolumeChunk(footstepsSound, MIX_MAX_VOLUME/8);
        }
        if(windowManager.isKeyPressed(SDLK_z)){            
            camera.moveFront(0.5);
            lastMove=SDL_GetTicks();
            if(currentWorld==0){
                camera.checkBorders(25.);
            }
        }
        if(windowManager.isKeyPressed(SDLK_s)){
            camera.moveFront(-0.5);
            lastMove=SDL_GetTicks();
            if(currentWorld==0){
                camera.checkBorders(25.);
            }
        }
        if(windowManager.isKeyPressed(SDLK_q)){
            camera.moveLeft(0.5);
            lastMove=SDL_GetTicks();
            if(currentWorld==0){
                camera.checkBorders(25.);
            }
        }
        if(windowManager.isKeyPressed(SDLK_d)){
            camera.moveLeft(-0.5);
            lastMove=SDL_GetTicks();
            if(currentWorld==0){
                camera.checkBorders(25.);
            }
        }
        if(windowManager.isMouseButtonPressed(SDL_BUTTON_LEFT)){
            camera.rotateUp((windowManager.getMousePosition().y-coordsMouse.y)/2);
            camera.rotateLeft((windowManager.getMousePosition().x-coordsMouse.x)/2);
        }

        coordsMouse=windowManager.getMousePosition();

        //Algoritmhe de jeu pour la collecte des feuilles dans chaque monde
        switch(currentWorld){
            case 0:
                if(scene.getLeaf(0)->getShown() && scene.getLeaf(0)->isClose(camera.getCameraPosition())){
                    if(windowManager.isKeyPressed(SDLK_SPACE)){
                        Mix_PlayChannel(-1, dingSound, 0);
                        scene.updateLeaf(0);                  
                    }
                }
                break;
            case 1:
                if(SDL_GetTicks()-lastMove>=10000 && !scene.getLeaf(1)->getShown() && !scene.getLeaf(1)->getCollected()){
                    scene.getLeaf(1)->setShown(true);
                }
                if(scene.getLeaf(1)->getShown() && scene.getLeaf(1)->isClose(camera.getCameraPosition())){
                    if(windowManager.isKeyPressed(SDLK_SPACE)){
                        Mix_PlayChannel(-1, dingSound, 0);
                        scene.updateLeaf(1);
                    }
                }
                break;
            case 2:
                if(scene.getLeaf(2)->getShown() && scene.getLeaf(2)->isClose(camera.getCameraPosition())){
                    if(windowManager.isKeyPressed(SDLK_SPACE)){
                        Mix_PlayChannel(-1, dingSound, 0);
                        scene.updateLeaf(2);
                    }
                }
                break;
            case 3:
                if(glm::distance(camera.getCameraPosition(), glm::vec3(-1033.,2.,-0.75))<=1 && !scene.getLeaf(3)->getCollected() && !scene.getLeaf(3)->getShown()){
                    for(float i=2.; i>-100.; i-=0.5){
                        camera.setCameraPosition(glm::vec3(camera.getCameraPosition().x,i,camera.getCameraPosition().z));
                    }
                    camera.setCameraPosition(scene.getGate(3)->getLandingPosWorld());
                    scene.getLeaf(3)->setShown(true);
                }
                if(scene.getLeaf(3)->getShown() && scene.getLeaf(3)->isClose(camera.getCameraPosition())){
                    if(windowManager.isKeyPressed(SDLK_SPACE)){
                        Mix_PlayChannel(-1, dingSound, 0);
                        scene.updateLeaf(3);
                    }
                }
                
                break;
            case 4:
                if(glm::distance(camera.getCameraPosition(), glm::vec3(-35.75,2.,-992.5))<=4 && !scene.getLeaf(4)->getShown() && SDL_GetTicks()-lastMove>=3000){
                    scene.getLeaf(4)->setShown(true);
                }
                if(scene.getLeaf(4)->getShown() && scene.getLeaf(4)->isClose(camera.getCameraPosition())){
                    if(windowManager.isKeyPressed(SDLK_SPACE)){
                        Mix_PlayChannel(-1, dingSound, 0);
                        scene.updateLeaf(4);
                    }
                }
                break;
        }

        if(currentWorld==0){
            gate=scene.checkGates(camera.getCameraPosition());
            if(gate!=-1 && gate !=0 && windowManager.isKeyPressed(SDLK_SPACE)){
                Mix_PlayChannel(-1, tpSound, 0);
                camera.setCameraPosition(scene.getGate(gate)->getLandingPosWorld());
                scene.stopMusic(currentWorld);
                currentWorld=gate;
            }
            if(gate==0 && scene.getNbLeavesCollected()==5){
                done=true;
            }
        }else{
            if(scene.getGate(currentWorld)->isInWorldGate(camera.getCameraPosition()) && windowManager.isKeyPressed(SDLK_SPACE)){
                Mix_PlayChannel(-1, tpSound, 0);
                camera.setCameraPosition(scene.getGate(currentWorld)->getLandingPosMain());
                scene.stopMusic(currentWorld);
                currentWorld=0;
            }
        }
        gate=-1;

        glClearColor(0./255,22./255,54./255,1.);
        //Nettoyage de la fen�tre � chaque tour
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glDepthMask(GL_FALSE);
        skyboxProgram.m_Program.use();

        MVMatrix = glm::mat4(1)*glm::mat4(glm::mat3(camera.getViewMatrix()));
        MVPMatrix = ProjMatrix*MVMatrix;

        glUniformMatrix4fv(skyboxProgram.uMVPMatrix,1,GL_FALSE,glm::value_ptr(MVPMatrix));
        glUniform1i(skyboxProgram.uSkybox,0);

        scene.drawSkybox(currentWorld);

        glDepthMask(GL_TRUE);

        if(currentWorld!=4){
            sceneProgram.m_Program.use(); 

            MVMatrix = glm::mat4(1)*camera.getViewMatrix();
            NormalMatrix = glm::transpose(glm::inverse(MVMatrix));
            MVPMatrix = ProjMatrix*MVMatrix;

            //Envoi des matrices au vertex shader
            glUniformMatrix4fv(sceneProgram.uMVPMatrix,1,GL_FALSE,glm::value_ptr(MVPMatrix));
            glUniformMatrix4fv(sceneProgram.uMVMatrix,1,GL_FALSE,glm::value_ptr(MVMatrix));
            glUniformMatrix4fv(sceneProgram.uNormalMatrix,1,GL_FALSE,glm::value_ptr(NormalMatrix));

            for(unsigned int i=0; i<NB_WORLDS; i++){
                scene.drawScene(i, sceneProgram.m_Program, camera.getViewMatrix());
            }
        }else{
            grotteProgram.m_Program.use(); 

            MVMatrix = glm::mat4(1)*camera.getViewMatrix();
            NormalMatrix = glm::transpose(glm::inverse(MVMatrix));
            MVPMatrix = ProjMatrix*MVMatrix;

            //Envoi des matrices au vertex shader
            glUniformMatrix4fv(grotteProgram.uMVPMatrix,1,GL_FALSE,glm::value_ptr(MVPMatrix));
            glUniformMatrix4fv(grotteProgram.uMVMatrix,1,GL_FALSE,glm::value_ptr(MVMatrix));
            glUniformMatrix4fv(grotteProgram.uNormalMatrix,1,GL_FALSE,glm::value_ptr(NormalMatrix));

            glUniform3fv(grotteProgram.uFlashlightPos, 1, glm::value_ptr(glm::vec3(MVMatrix*glm::vec4(camera.getCameraPosition(),1.))));
            glUniform3fv(grotteProgram.uFlashlightDir, 1, glm::value_ptr(camera.getFrontVector()+camera.getCameraPosition()));

            glUniform1f(grotteProgram.uFlashlightCutoff,glm::cos(glm::radians(5.f)));
            glUniform1f(grotteProgram.uFlashlightOuterCutoff,glm::cos(glm::radians(45.f)));

            glUniform1f(grotteProgram.uConstant,1.f);
            glUniform1f(grotteProgram.uLinear,0.05f);
            glUniform1f(grotteProgram.uQuadratic,0.01f);
                
            for(unsigned int i=0; i<NB_WORLDS; i++){
                scene.drawScene(i, grotteProgram.m_Program, camera.getViewMatrix());
            }
        }
        
        scene.playMusic(currentWorld);

        // Update the display
        windowManager.swapBuffers();
    }

    std::cout << "Well done, you just woke up ... :)" << std::endl;
    scene.deleteData();
    
    SDL_Quit();
    return EXIT_SUCCESS;
}

