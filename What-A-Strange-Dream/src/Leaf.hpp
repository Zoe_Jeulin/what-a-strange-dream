#pragma once

#include "WorldObject.hpp"
#include <glm/glm.hpp>
#include <math.h> 
#include <iostream>

class Leaf : public WorldObject{
	private:
	    glm::vec3 leafPos;
        int indexModel;
        bool shown, collected;

    public:
    	Leaf();
    	Leaf(unsigned int id, glm::vec3 pos, int index, bool shown, bool collected, float offsetX, float offsetZ);
    	Leaf(const Leaf &lf);
        int getIndexModel();
        void setIndexModel(int index);
        bool getShown();
        void setShown(bool s);
        bool getCollected();
        void setCollected(bool c);
        bool isClose(glm::vec3 posCamera);
};