#pragma once

#include <GL/glew.h>

struct Material{
    glm::vec4 Ka; //Ambient color
    glm::vec4 Kd; //Diffuse color
    glm::vec4 Ks; //Specular color
};