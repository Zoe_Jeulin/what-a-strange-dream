#pragma once

#include <string>
#include <iostream>
#include "SDL_mixer.h"
#include "WorldObject.hpp"

class Music : public WorldObject{
	private:
	    Mix_Music *music;

    public:
    	Music();
    	Music(unsigned int id, std::string const &path, float offsetX, float offsetZ);
    	Music(const Music &mu);
    	Mix_Music* getMusic();
        void play();
        void stop();
};