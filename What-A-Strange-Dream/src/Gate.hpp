#pragma once

#include "WorldObject.hpp"
#include <glm/glm.hpp>
#include <math.h> 
#include <iostream>

class Gate : public WorldObject{
	private:
	    float posXmain; //centre du portail dans le monde principal
	    float posZmain;
	    float radius;
	    float posXworld; //centre du portail dans son monde
	    float posZworld;

    public:
    	Gate();
    	Gate(unsigned int id, float xMain, float zMain, float r, float xWorld, float zWorld, float offsetX, float offsetZ);
    	Gate(const Gate &g);
    	int isInMainGate(glm::vec3 posCamera);
    	bool isInWorldGate(glm::vec3 posCamera);
    	glm::vec3 getLandingPosWorld();
    	glm::vec3 getLandingPosMain();
};