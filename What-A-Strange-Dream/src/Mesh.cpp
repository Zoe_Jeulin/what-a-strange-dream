#include "Mesh.hpp"

Mesh::Mesh(std::vector<Vertex> &vertices, std::vector<unsigned int> &indices, std::vector<Texture> &textures, Material &material){
	this->vertices=vertices;
	this->indices=indices;
	this->textures=textures;
	this->material=material;

	setupMesh();
}

//Créé le mesh
void Mesh::setupMesh(){
	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &IBO);
    glGenBuffers(1, &UBO);

    //VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);  

    //VAO
    glBindVertexArray(VAO);
    //Vertex positions
    glEnableVertexAttribArray(0);	
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    //Vertex normals
    glEnableVertexAttribArray(1);	
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
    //Vertex texture coords
    glEnableVertexAttribArray(2);	
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

    //IBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

    //UBO
	glBindBuffer(GL_UNIFORM_BUFFER, UBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(material),(void*)(&material), GL_STATIC_DRAW);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);	    
}

//Dessine le mesh
void Mesh::drawMesh(glimac::Program &program){
    glBindVertexArray(VAO);
	glBindBufferRange(GL_UNIFORM_BUFFER,0, UBO, 0, sizeof(Material));
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

GLuint* Mesh::getVAO(){
    return &VAO;
}

GLuint* Mesh::getVBO(){
    return &VBO;
}