#pragma once

#include "Model.hpp"
#include "Light.hpp"
#include "Skybox.hpp"
#include "Music.hpp"
#include "Gate.hpp"
#include "Leaf.hpp"
#include "World.hpp"
#include <glimac/Program.hpp>
#include <glimac/common.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <filesystem>
namespace fs = std::filesystem;

class Scene{

    private:
        std::vector<Model> models;
        std::vector<Light> lights;
        std::vector<Skybox> skyboxes;
        std::vector<Music> musics;
        std::vector<Gate> gates;
        std::vector<Leaf> leaves;
        std::vector<World> worlds;
        std::vector<glm::vec3> offsets;

        void loadScene(std::string const &path);
        void createWorlds();

    public:
        Scene(std::string const &path);
        void drawScene(unsigned int idWorld, glimac::Program &program, glm::mat4 const &viewMatrix);
        void drawSkybox(unsigned int idWorld);
        void playMusic(unsigned int idWorld);
        void stopMusic(unsigned int idWorld);
        int checkGates(glm::vec3 posCamera);
        Gate* getGate(int idGate);
        Leaf* getLeaf(int idLeaf);
        void updateLeaf(int i);
        int getNbLeavesCollected();
        void deleteData();
        
};