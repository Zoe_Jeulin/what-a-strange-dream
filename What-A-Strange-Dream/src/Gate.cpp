#include "Gate.hpp"

Gate::Gate(){}

Gate::Gate(unsigned int id, float xMain, float zMain, float r, float xWorld, float zWorld, float offsetX, float offsetZ):WorldObject(id, offsetX, offsetZ){
	this->worldId=id;
	this->posXmain=xMain;
	this->posZmain=zMain;
	this->radius=r;
	this->posXworld=xWorld+offsetX;
	this->posZworld=zWorld+offsetZ;
	this->worldOffsetX=offsetX;
	this->worldOffsetZ=offsetZ;
}

Gate::Gate(const Gate &g):WorldObject(g){
	this->worldId=g.worldId;
	this->posXmain=g.posXmain;
	this->posZmain=g.posZmain;
	this->radius=g.radius;
	this->posXworld=g.posXworld;
	this->posZworld=g.posZworld;
	this->worldOffsetX=g.worldOffsetX;
	this->worldOffsetZ=g.worldOffsetZ;
}

//Si le joueur se trouve dans la zone d'un portail (dans le monde principal), retourne l'id du monde concerné
int Gate::isInMainGate(glm::vec3 posCamera){
	if(glm::distance(glm::vec2(posXmain,posZmain),glm::vec2(posCamera.x,posCamera.z))<radius){
		return worldId;
	}else{
		return -1;
	}
}

//Retourne un booléen indiquant si le joueur se trouve dans la zone du portail, dans chacun des monde (hormis le principal)
bool Gate::isInWorldGate(glm::vec3 posCamera){
	if(glm::distance(glm::vec2(posXworld,posZworld),glm::vec2(posCamera.x,posCamera.z))<radius){
		return true;
	}else{
		return false;
	}
}

glm::vec3 Gate::getLandingPosWorld(){
	return glm::vec3(posXworld, 2., posZworld);
}

glm::vec3 Gate::getLandingPosMain(){
	return glm::vec3(posXmain, 2., posZmain);
}