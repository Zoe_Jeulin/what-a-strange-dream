#include "Music.hpp"

Music::Music(){}

Music::Music(unsigned int id, std::string const &path, float offsetX, float offsetZ):WorldObject(id, offsetX, offsetZ){
	this->worldId=id;
	this->music=Mix_LoadMUS(path.c_str());
	if(music == NULL){
        std::cerr << "Erreur au chargement de la musique " << path << std::endl;
    }
    this->worldOffsetX=offsetX;
    this->worldOffsetZ=offsetZ;
}

Music::Music(const Music &mu):WorldObject(mu){
	this->worldId=mu.worldId;
	this->music=mu.music;
	this->worldOffsetX=mu.worldOffsetX;
	this->worldOffsetZ=mu.worldOffsetZ;
}

Mix_Music* Music::getMusic(){
	return music;
}

//Joue la musique
void Music::play(){
	if(Mix_PlayingMusic() == 0 ){ //Si la musique n'est pas déjà lancée
        Mix_PlayMusic(music,-1);
    }
}

//Arrête la musique
void Music::stop(){
	Mix_FadeOutMusic(1000);
}