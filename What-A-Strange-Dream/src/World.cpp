#include "World.hpp"

World::World(unsigned int id, std::vector<Model> &models, std::vector<Light> &lights, Skybox &skybox, Music &music){
	this->worldId = id;
	this->worldModels = models;
	this->worldLights = lights;
	this->worldSkybox = skybox;
	this->worldMusic = music;
}

//Dessine le contenu d'un monde (lumières, models, skybox)
void World::drawWorld(glimac::Program &program, glm::mat4 const &viewMatrix, std::vector<Leaf> &leaves){	
	for(unsigned int i=0; i<worldLights.size(); i++){
		worldLights[i].drawLight(program, viewMatrix, i);
	}

	if(worldId==0){
		for(int i=0; i<worldModels.size(); i++){
			if((i!=leaves[0].getIndexModel() || leaves[0].getShown()) &&
				(i!=leaves[5].getIndexModel() || leaves[5].getShown()) &&
				(i!=leaves[6].getIndexModel() || leaves[6].getShown()) &&
				(i!=leaves[7].getIndexModel() || leaves[7].getShown()) &&
				(i!=leaves[8].getIndexModel() || leaves[8].getShown()) &&
				(i!=leaves[9].getIndexModel() || leaves[9].getShown())){
					worldModels[i].drawModel(program);
			}
		}
	}else{
		for(int i=0; i<worldModels.size(); i++){
			if(i!=leaves[worldId].getIndexModel() || leaves[worldId].getShown()){
				worldModels[i].drawModel(program);
			}			
		}
	}
}