#include "Skybox.hpp"

Skybox::Skybox(){}

Skybox::Skybox(unsigned int id, std::string const &path, float offsetX, float offsetZ):WorldObject(id, offsetX, offsetZ){
    this->worldId = id;
    this->worldOffsetX=offsetX;
    this->worldOffsetZ=offsetZ;
    loadSkybox(path);
}

Skybox::Skybox(const Skybox &sb):WorldObject(sb){
	this->worldId=sb.worldId;
	this->textures=sb.textures;
	this->textureSkybox=sb.textureSkybox;
	this->skyboxVAO=sb.skyboxVAO;
	this->skyboxVBO=sb.skyboxVBO;
    this->worldOffsetX=sb.worldOffsetX;
    this->worldOffsetZ=sb.worldOffsetZ;
}

//Charge les textures de la skybox et la crée
void Skybox::loadSkybox(std::string const &path){
	glGenVertexArrays(1, &skyboxVAO);
    glGenBuffers(1, &skyboxVBO);
    glBindVertexArray(skyboxVAO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), 0);

	glimac::Image *rightTexture = glimac::loadImage(path+"/right.jpg");
    if(rightTexture==nullptr){
        std::cerr << "Erreur au chargement de la texture " << path << "/right.jpg" << std::endl;
    }
    
    glimac::Image *leftTexture = glimac::loadImage(path+"/left.jpg");
    if(leftTexture==nullptr){
        std::cerr << "Erreur au chargement de la texture " << path << "/left.jpg" << std::endl;
    }

    glimac::Image *topTexture = glimac::loadImage(path+"/top.jpg");
    if(topTexture==nullptr){
        std::cerr << "Erreur au chargement de la texture " << path << "/top.jpg" << std::endl;
    }

    glimac::Image *bottomTexture = glimac::loadImage(path+"/bottom.jpg");
    if(bottomTexture==nullptr){
        std::cerr << "Erreur au chargement de la texture " << path << "/bottom.jpg" << std::endl;
    }

    glimac::Image *frontTexture = glimac::loadImage(path+"/front.jpg");
    if(frontTexture==nullptr){
        std::cerr << "Erreur au chargement de la texture " << path << "/front.jpg" << std::endl;
    }

    glimac::Image *backTexture = glimac::loadImage(path+"/back.jpg");
    if(backTexture==nullptr){
        std::cerr << "Erreur au chargement de la texture " << path << "/back.jpg" << std::endl;
    }    

    textures.push_back(std::move(rightTexture));
    textures.push_back(std::move(leftTexture));
    textures.push_back(std::move(topTexture));
    textures.push_back(std::move(bottomTexture));
    textures.push_back(std::move(frontTexture));
    textures.push_back(std::move(backTexture));

    glGenTextures(1, &textureSkybox);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureSkybox);

    for(unsigned int i=0; i<textures.size(); i++){
    	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, GL_RGBA, (*textures[i]).getWidth(), (*textures[i]).getHeight(), 0, GL_RGBA, GL_FLOAT, (*textures[i]).getPixels());
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

//Dessine la skybox
void Skybox::drawSkybox(){
	glBindVertexArray(skyboxVAO);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureSkybox);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

GLuint* Skybox::getVAO(){
    return &skyboxVAO;
}

GLuint* Skybox::getVBO(){
    return &skyboxVBO;
}