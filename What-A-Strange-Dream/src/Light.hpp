#pragma once

#include <GL/glew.h>
#include <iostream>
#include <glimac/glm.hpp>
#include <glimac/Program.hpp>
#include <string>
#include "WorldObject.hpp"

class Light : public WorldObject{

    private:
        glm::vec3 lightPos;
        glm::vec3 coordsPos;
        glm::vec3 lightIntensity;

    public:
        Light(unsigned int id, glm::vec3 coords, glm::vec3 intensity, float offsetX, float offsetZ);
        void drawLight(glimac::Program &program, glm::mat4 const &viewMatrix, unsigned int i);
};