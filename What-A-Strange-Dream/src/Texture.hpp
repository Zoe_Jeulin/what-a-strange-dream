#pragma once

#include <GL/glew.h>
#include <string>

struct Texture {
    GLuint id;
    std::string type;
    std::string path;  // we store the path of the texture to compare with other textures
};