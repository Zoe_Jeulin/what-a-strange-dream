#pragma once

#include <string>

#define WINDOW_WIDTH 1200
#define WINDOW_HEIGHT 800

#define WINDOW_TITLE "What A Strange Dream"

#define PATH_ASSETS "../What-A-Strange-Dream/assets/"
#define PATH_SHADERS PATH_ASSETS "shaders/"
#define PATH_MODELS PATH_ASSETS "models/"
#define PATH_MUSICS PATH_ASSETS "musics/"
#define PATH_TEXTURES PATH_ASSETS "textures/"
#define PATH_SKYBOXES PATH_TEXTURES "skyboxes/"

#define NB_WORLDS 5
