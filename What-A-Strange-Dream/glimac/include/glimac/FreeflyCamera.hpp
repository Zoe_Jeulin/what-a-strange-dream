#pragma once
#include <iostream>
#include <math.h> 

using namespace std;

namespace glimac {

    class FreeflyCamera {

        private:
            glm::vec3 m_Position;
            float m_fPhi;
            float m_fTheta;
            glm::vec3 m_FrontVector;
            glm::vec3 m_LeftVector;
            glm::vec3 m_UpVector;

            inline void computeDirectionVectors(){
                m_FrontVector=glm::vec3(cos(m_fTheta)*sin(m_fPhi), sin(m_fTheta), cos(m_fTheta)*cos(m_fPhi));
                m_LeftVector=glm::vec3(sin(m_fPhi+glm::pi<float>()/2), 0, cos(m_fPhi+glm::pi<float>()/2));
                m_UpVector=glm::cross(m_FrontVector,m_LeftVector);
            }

        public:
            FreeflyCamera(){
                m_Position=glm::vec3(-0.34,2.,-7.2);
                m_fPhi=0;
                m_fTheta=0;
                computeDirectionVectors();
            }

            //Se déplacer sur l'axe gauche-droite
            inline void moveLeft(float t){
                m_Position+=t*m_LeftVector;
                m_Position.y=2.;
            }

            //Se déplacer sur l'axe devant-derrière
            inline void moveFront(float t){
                m_Position+=t*m_FrontVector;
                m_Position.y=2.;
            }

            //Rotation de la caméra sur l'angle gauche-droite
            inline void rotateLeft(float degrees){
                m_fPhi+=glm::radians(degrees);
                computeDirectionVectors();
            }

            //Rotation de la caméra sur l'angle haut-bas
            inline void rotateUp(float degrees){
                m_fTheta+=glm::radians(degrees);
                computeDirectionVectors();
            }

            inline glm::mat4 getViewMatrix()const{
                return glm::lookAt(m_Position,m_Position+m_FrontVector,m_UpVector);
            }

            inline glm::vec3 getCameraPosition(){
                return m_Position;
            }

            inline void setCameraPosition(glm::vec3 newPos){
                m_Position=newPos;
            }

            inline glm::vec3 getFrontVector(){
                return m_FrontVector;
            }

            //Vérifie que le joueur ne dépasse pas le bord de l'îlot principal
            inline void checkBorders(float radius){
                float distance = sqrt(pow(m_Position.x,2)+pow(m_Position.z,2));
                if(distance>radius){
                    m_Position.x=m_Position.x*(radius/distance);
                    m_Position.z=m_Position.z*(radius/distance);
                }
            }        
    };
}
