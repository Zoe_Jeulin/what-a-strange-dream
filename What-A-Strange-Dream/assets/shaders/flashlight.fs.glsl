#version 300 es

precision mediump float;

in vec3 vPosition;
in vec3 vNormal;

uniform Mat{
	vec4 aAmbient;
	vec4 aDiffuse;
	vec4 aSpecular;
};

uniform vec3 uLightPos;
uniform vec3 uLightIntensity;

uniform vec3 uLightDir;
uniform vec3 uFlashlightPos;
uniform vec3 uFlashlightDir;
uniform float uFlashlightCutoff;
uniform float uFlashlightOuterCutoff;

uniform float uConstant;
uniform float uLinear;
uniform float uQuadratic;

out vec3 fFragColor;

/*vec3 flashlight(){
	vec3 norm = normalize(vNormal);
	vec3 lightDir = normalize(uFlashlightPos-vPosition);
	vec3 reflectDir = reflect(-lightDir, norm);
		
	vec3 ambient = vec3(aAmbient);
	vec3 diffuse = vec3(aDiffuse)*max(dot(norm,lightDir),0.);
	vec3 specular = vec3(aSpecular)*max(dot(lightDir,reflectDir),0.);
	
	float theta = dot(lightDir, normalize(-uFlashlightDir));
	
	
	
	if(theta > uFlashlightCutoff){
		return 0.7*vec3(aAmbient)+li*(diffuse*1.5+specular*0.8);
	}else{
		return vec3(0.,0.,0.);
		//return 0.3*vec3(aAmbient)+li*(diffuse+specular*0.8);
	}
}*/

vec3 flashlight(){
	vec3 norm = normalize(vNormal);
	vec3 lightDir = normalize(uFlashlightPos-vPosition);
	vec3 reflectDir = reflect(-lightDir, norm);
		
	vec3 ambient = 0.6*vec3(aAmbient);
	vec3 diffuse = vec3(aDiffuse)*max(dot(norm,lightDir),0.);
	vec3 specular = 0.8*vec3(aSpecular)*max(dot(lightDir,reflectDir),0.);
	
	float theta = dot(lightDir, normalize(-uFlashlightDir));
	float epsilon = uFlashlightCutoff-uFlashlightOuterCutoff;
	float intensity = clamp((theta-uFlashlightOuterCutoff)/epsilon, 0., 1.);
	
	ambient*=(intensity*1.2);
	diffuse*=(intensity*0.8);
	specular*=(intensity*0.8);
	
	float distance = length(uFlashlightPos-vPosition)/2.;
    	float attenuation = 1./(uConstant+uLinear*distance+uQuadratic*(distance*distance));
    	    
    	ambient *= attenuation; 
    	diffuse *= (attenuation*1.5);
    	specular *= (attenuation*1.5); 
    	
	return ambient+diffuse+specular;
}


void main() {
	fFragColor = flashlight();
};
