#version 300 es

precision mediump float;

in vec3 vPosition;
in vec3 vNormal;

uniform Mat{
	vec4 aAmbient;
	vec4 aDiffuse;
	vec4 aSpecular;
};

//uniform vec3 uKd; //coefficient de réflexion diffuse de l'objet (couleur qui dépend du matériau de l'objet)
//uniform vec3 uKs; //coefficient de réflexion spéculaire de l'objet (couleur qui dépend du matérieu de l'objet)
//uniform float uShininess; //exposant de brillance permettant de controller la taille de la tâche de brillance glossy
uniform vec3 uLightPos1; //Wi 
uniform vec3 uLightPos2;
//uniform vec3 uLightIntensity; //Li

out vec3 fFragColor;

vec3 blinnPhong1(){
	vec3 lightIntensity = vec3(10000.,10000.,10000.);
	vec3 li = lightIntensity/pow(distance(uLightPos1,vPosition),2.);
	vec3 diffuse = vec3(aDiffuse)*(max(dot(normalize(uLightPos1-vPosition),normalize(vNormal)),0.));
	//vec3 specular = vec3(aSpecular)*pow(max(dot(normalize((normalize(-vPosition)+normalize(uLightPos-lightIntensity))/2.),normalize(vNormal)),0.),2.);
	vec3 specular = vec3(aSpecular)*max(dot(normalize((normalize(-vPosition)+normalize(uLightPos1-lightIntensity))/2.),normalize(vNormal)),0.);
	
	return li*(diffuse);
}

vec3 blinnPhong2(){
	vec3 lightIntensity = vec3(100.,100.,100.);
	vec3 li = lightIntensity/pow(distance(uLightPos2,vPosition),2.);
	vec3 diffuse = vec3(aDiffuse)*(max(dot(normalize(uLightPos2-vPosition),normalize(vNormal)),0.));
	//vec3 specular = vec3(aSpecular)*pow(max(dot(normalize((normalize(-vPosition)+normalize(uLightPos-lightIntensity))/2.),normalize(vNormal)),0.),2.);
	vec3 specular = vec3(aSpecular)*max(dot(normalize((normalize(-vPosition)+normalize(uLightPos2-lightIntensity))/2.),normalize(vNormal)),0.);
	
	return li*(diffuse);
}


void main() {
	fFragColor = blinnPhong1();
	fFragColor += blinnPhong2();
};
