#version 300 es

precision mediump float;

uniform vec3 uLightPos;
uniform vec3 uViewPos;

in vec3 vPosition;
in vec3 vNormal;

in vec4 vAmbient;
in vec4 vDiffuse;
in vec4 vSpecular;

out vec3 fFragColor;

void main() {
	vec3 lightColor = vec3(1.,1.,1.);
	
	float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

	vec3 norm = normalize(vNormal);
	vec3 lightDir = normalize(uLightPos-vPosition);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;
	
	float specularStrength = 0.5;
	vec3 viewDir = normalize(uViewPos - vPosition);
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 256.);
	vec3 specular = specularStrength * spec * lightColor; 
	
	//vec3 result = (vAmbient + diffuse) * objectColor;
	fFragColor = (diffuse+ambient)*vDiffuse.xyz;
};
