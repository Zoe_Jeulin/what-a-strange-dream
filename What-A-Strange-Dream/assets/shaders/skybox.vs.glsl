#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;

uniform mat4 uMVPMatrixSkybox;

out vec3 vTexCoords;

void main() {
	vTexCoords = aVertexPosition;
	gl_Position = uMVPMatrixSkybox*vec4(aVertexPosition,1);
};
