#version 300 es

precision mediump float;

in vec3 vPosition;
in vec3 vNormal;

uniform Mat{
	vec4 aAmbient;
	vec4 aDiffuse;
	vec4 aSpecular;
};

uniform vec3 uLightPos;

uniform vec3 uLightIntensity;

out vec3 fFragColor;

vec3 blinnPhong(){
	vec3 li = uLightIntensity/pow(distance(uLightPos,vPosition),2.);
	vec3 diffuse = vec3(aDiffuse)*(max(dot(normalize(uLightPos-vPosition),normalize(vNormal)),0.));
	//vec3 specular = vec3(aSpecular)*pow(max(dot(normalize((normalize(-vPosition)+normalize(uLightPos0-uLightIntensity0))/2.),normalize(vNormal)),0.),2.);
	vec3 specular = vec3(aSpecular)*max(dot(normalize((normalize(-vPosition)+normalize(uLightPos-uLightIntensity))/2.),normalize(vNormal)),0.);
	
	//return li*(diffuse);
	//return li*(diffuse+specular);
	return 0.7*vec3(aAmbient)+li*(diffuse*1.5+specular*0.8);
}

void main() {
	fFragColor = blinnPhong();
};
