#version 300 es

precision mediump float;

in vec3 vTexCoords;

uniform samplerCube uSkybox;

out vec4 fFragColor;

void main(){    
    fFragColor = texture(uSkybox, vTexCoords);
}
